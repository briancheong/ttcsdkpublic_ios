#
# Be sure to run `pod lib lint TTCSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'TTCSDK'
s.version          = '0.0.24'
s.summary          = 'TTCSDK'
s.homepage         = 'https://bitbucket.org/briancheong/ttcsdkpublic_ios'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'tataufo' => 'tataufo@tataufo.com' }
s.source           = { :git => 'https://bitbucket.org/briancheong/ttcsdkpublic_ios.git', :tag => s.version.to_s }

s.ios.deployment_target = '9.0'

s.swift_version    = "4.1"
s.platform         = :ios
s.ios.deployment_target = '9.0'
s.frameworks = "Foundation", 'UIKit'
s.vendored_frameworks = 'Framework/TTCSDK.framework', 'Framework/TTC_SDK_NET.framework'
s.resources = 'Resources/TTCSDKBundle.bundle'

s.dependency 'SwiftProtobuf', '1.0.3'
s.dependency 'BigInt', '3.1.0'
s.dependency 'JSONRPCKit', '3.0.0'
s.dependency 'APIKit', '3.2.1'
s.dependency 'CryptoSwift', '0.10.0'
s.dependency 'TrustCore', '0.0.7'
s.dependency 'TrezorCrypto', '0.0.6'
s.dependency 'RealmSwift', '3.7.5'

end
